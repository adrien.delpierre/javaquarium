package fr.javaquarium.objets;

public class PoissonClown extends PoissonCarnivore {

	public PoissonClown() {
		super();
	}

	public PoissonClown(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie, nom, sexe);
	}

	public PoissonClown(String nom, Sexe sexe) {
		super(nom, sexe);
	}
	
	

}
