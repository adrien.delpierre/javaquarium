package fr.javaquarium.objets;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Aquarium {
	
	private List<EspeceAquatique> especesAquatiques = new ArrayList<>();
	
	public Aquarium() {
		
	}

	public Aquarium(List<EspeceAquatique> especesAquatiques) {
		super();
		this.especesAquatiques = especesAquatiques;
	}

	public List<EspeceAquatique> getEspecesAquatiques() {
		return especesAquatiques;
	}

	public void setEspecesAquatiques(List<EspeceAquatique> especesAquatiques) {
		this.especesAquatiques = especesAquatiques;
	}
	
	public void ajouterPoisson(Poisson poisson) {
		this.especesAquatiques.add(poisson);
	}
	
	public void ajouterAlgue(Algue algue) {
		this.especesAquatiques.add(algue);
	}
	
	private List<Poisson> getPoissons() {
		return especesAquatiques.stream().filter(e -> e instanceof Poisson).map(Poisson.class::cast).toList();
	}
	
	private List<Algue> getAlgues() {
		return especesAquatiques.stream().filter(e -> e instanceof Algue).map(Algue.class::cast).toList();
	}
	
	public void passerLeTemps() {
		// on fait les tours des poissons avant le tour des algues
		for (Poisson poisson : getPoissons()) {
			
		}
	}
	
	

}
