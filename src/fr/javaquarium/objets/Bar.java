package fr.javaquarium.objets;

public class Bar extends PoissonCarnivore {

	public Bar() {
		super();
	}

	public Bar(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie, nom, sexe);
	}

	public Bar(String nom, Sexe sexe) {
		super(nom, sexe);
	}
	
	

}
