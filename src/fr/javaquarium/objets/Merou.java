package fr.javaquarium.objets;

public class Merou extends PoissonCarnivore {

	public Merou() {
		super();
	}

	public Merou(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie, nom, sexe);
	}

	public Merou(String nom, Sexe sexe) {
		super(nom, sexe);
	}
	
	

}
