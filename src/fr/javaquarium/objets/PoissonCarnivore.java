package fr.javaquarium.objets;

public abstract class PoissonCarnivore extends Poisson {

	public PoissonCarnivore() {
		super();
	}

	public PoissonCarnivore(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie, nom, sexe);
	}

	public PoissonCarnivore(String nom, Sexe sexe) {
		super(nom, sexe);
	}
	
	public void manger(EspeceAquatique especeAquatique) {
		
	}
	
	

}
