package fr.javaquarium.objets;

public class PoissonChirurgien extends PoissonHerbivore {

	public PoissonChirurgien() {
		super();
	}

	public PoissonChirurgien(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie, nom, sexe);
	}

	public PoissonChirurgien(String nom, Sexe sexe) {
		super(nom, sexe);
	}
	
	

}
