package fr.javaquarium.objets;

public abstract class Poisson<EspeceMangeable extends EspeceAquatique> extends EspeceAquatique {
	
	private String nom;
	private Sexe sexe;
	
	public Poisson() {
		super();
	}
	
	public Poisson(String nom, Sexe sexe) {
		super();
		this.nom = nom;
		this.sexe = sexe;
	}

	public Poisson(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie);
		this.nom = nom;
		this.sexe = sexe;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Sexe getSexe() {
		return sexe;
	}

	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}

	public abstract void manger(EspeceMangeable especeMangeable);
	
	@Override
	public String toString() {
		return "Poisson [getNom()=" + getNom() + ", getSexe()=" + getSexe() + ", getPointDeVie()=" + getPointDeVie()
				+ "]";
	}
	
	
	
	
}
