package fr.javaquarium.objets;

public class Thon extends PoissonCarnivore {

	public Thon() {
		super();
	}

	public Thon(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie, nom, sexe);
	}

	public Thon(String nom, Sexe sexe) {
		super(nom, sexe);
	}
	
	

}
