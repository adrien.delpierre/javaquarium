package fr.javaquarium.objets;

public abstract class PoissonHerbivore extends Poisson<Algue> {

	public PoissonHerbivore() {
		super();
	}

	public PoissonHerbivore(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie, nom, sexe);
	}

	public PoissonHerbivore(String nom, Sexe sexe) {
		super(nom, sexe);
	}
	
	public void manger(Algue algue) {
		
	}
	
	

}
