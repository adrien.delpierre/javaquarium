package fr.javaquarium.objets;

public class Saupe extends PoissonHerbivore {

	public Saupe() {
		super();
	}

	public Saupe(int pointDeVie, String nom, Sexe sexe) {
		super(pointDeVie, nom, sexe);
	}

	public Saupe(String nom, Sexe sexe) {
		super(nom, sexe);
	}
	
	

}
