package fr.javaquarium.objets;

public abstract class EspeceAquatique {
	
	private int pointDeVie;
	
	public EspeceAquatique() {
		this(10);
	}

	public EspeceAquatique(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}

	public int getPointDeVie() {
		return pointDeVie;
	}

	public void setPointDeVie(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}

	@Override
	public String toString() {
		return "EspeceAquatique [pointDeVie=" + pointDeVie + "]";
	}
	

}
