package fr.javaquarium;

import java.util.ArrayList;
import java.util.List;

import fr.javaquarium.objets.Algue;
import fr.javaquarium.objets.Aquarium;
import fr.javaquarium.objets.Bar;
import fr.javaquarium.objets.EspeceAquatique;
import fr.javaquarium.objets.Merou;
import fr.javaquarium.objets.Poisson;
import fr.javaquarium.objets.PoissonChirurgien;
import fr.javaquarium.objets.PoissonClown;
import fr.javaquarium.objets.Saupe;
import fr.javaquarium.objets.Sexe;
import fr.javaquarium.objets.Thon;

public class Launcher {
	
	public static void main(String[] args) {
		
		Aquarium javaquarium = new Aquarium();
		initialisationAquarium(javaquarium);
		
		javaquarium.passerLeTemps();
		afficherEtat(javaquarium);

	}

	private static void afficherEtat(Aquarium javaquarium) {
		for (EspeceAquatique especeAquatique : javaquarium.getEspecesAquatiques()) {
			System.out.print(especeAquatique.getClass().getSimpleName());
			System.out.print(" : ");
			if(especeAquatique instanceof Poisson) {
				System.out.print(((Poisson) especeAquatique).getNom());
				System.out.print(" - ");
				System.out.print(((Poisson) especeAquatique).getSexe());
				System.out.print(" - ");
			}
			System.out.println(especeAquatique.getPointDeVie() + " PV.");
		}
	}

	private static void initialisationAquarium(Aquarium javaquarium) {
		Poisson merou = new Merou("Georges le m�rou", Sexe.MALE);
		Poisson merulette = new Merou("M�rulette", Sexe.FEMELLE);
		Poisson dany = new Merou("Dany le m�rou", Sexe.MALE);
		Poisson barMitzvah = new Bar("Bar Mitzvah", Sexe.MALE);
		Poisson thonelle = new Thon("Thonelle", Sexe.FEMELLE);
		Poisson nemo = new PoissonClown("Nemo", Sexe.MALE);
		Poisson marin = new PoissonClown("Marin", Sexe.MALE);
		Poisson saupeUI = new Saupe("Saupe UI", Sexe.FEMELLE);
		Poisson saupeErfort = new Saupe("Saupe Erfort", Sexe.MALE);
		Poisson malalatete = new PoissonChirurgien("Malalatete", Sexe.MALE);
		
		javaquarium.ajouterPoisson(merou);
		javaquarium.ajouterPoisson(merulette);
		javaquarium.ajouterPoisson(dany);
		javaquarium.ajouterPoisson(barMitzvah);
		javaquarium.ajouterPoisson(thonelle);
		javaquarium.ajouterPoisson(nemo);
		javaquarium.ajouterPoisson(marin);
		javaquarium.ajouterPoisson(saupeUI);
		javaquarium.ajouterPoisson(saupeErfort);
		javaquarium.ajouterPoisson(malalatete);
		javaquarium.ajouterAlgue(new Algue());
		javaquarium.ajouterAlgue(new Algue());
		javaquarium.ajouterAlgue(new Algue());
		javaquarium.ajouterAlgue(new Algue());
	}

}
